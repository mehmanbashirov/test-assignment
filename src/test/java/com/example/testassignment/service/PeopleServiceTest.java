package com.example.testassignment.service;

import com.example.testassignment.mapper.PeopleMapper;
import com.example.testassignment.model.dto.People;
import com.example.testassignment.model.dto.PeopleResponseDto;
import com.example.testassignment.model.entity.PeopleEntity;
import com.example.testassignment.repository.PeopleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("People service tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {PeopleService.class})
class PeopleServiceTest {

    @MockBean
    private PeopleRepository peopleRepository;

    @MockBean
    private PeopleMapper peopleMapper;

    @Autowired
    private PeopleService peopleService;

    private final Long COUNT = 500L;
    private PeopleEntity peopleEntity;
    private List<People> peopleList;

    @BeforeEach
    public void setup() {
        String NAME = "TEST";
        String URL = "http://test";
        peopleEntity = PeopleEntity.builder()
                .id(1L)
                .name(NAME)
                .url(URL)
                .build();
        peopleList = List.of(People.builder()
                .name(NAME)
                .url(URL)
                .build());
    }

    @Test
    @DisplayName("when getPeopleList then should return List of PeopleDto")
    void getPeopleList() {
        var expected = PeopleResponseDto.builder()
                .count(COUNT)
                .peopleList(peopleList)
                .build();

        when(peopleRepository.count())
                .thenReturn(COUNT);

        when(peopleRepository.findBy(null))
                .thenReturn(List.of(peopleEntity));

        when(peopleMapper.toPeopleDtoList(anyList()))
                .thenReturn(peopleList);


        PeopleResponseDto actual = peopleService.getPeopleList(null);

        assertThat(expected).isEqualTo(actual);
    }

    @Test
    @DisplayName("when getPeopleList then should return List of PeopleDto")
    void getPeopleListWithNullResponse() {
        var expected = PeopleResponseDto.builder()
                .count(0L)
                .peopleList(Collections.emptyList())
                .build();

        when(peopleRepository.count())
                .thenReturn(0L);

        when(peopleRepository.findBy(null))
                .thenReturn(Collections.emptyList());

        when(peopleMapper.toPeopleDtoList(anyList()))
                .thenReturn(Collections.emptyList());


        PeopleResponseDto actual = peopleService.getPeopleList(null);

        assertThat(expected).isEqualTo(actual);
    }


    @Test
    @DisplayName("when getPeopleList then should return List of PeopleDto")
    void getPeopleListE() {
        var expected = PeopleResponseDto.builder()
                .count(COUNT)
                .peopleList(peopleList)
                .build();

        when(peopleRepository.findBy(null))
                .thenReturn(List.of(peopleEntity));

        when(peopleMapper.toPeopleDtoList(anyList()))
                .thenReturn(peopleList);

        when(peopleRepository.countByName(any()))
                .thenReturn(COUNT);

        PeopleResponseDto actual = peopleService.findByName(anyString(), null);

        assertThat(expected).isEqualTo(actual);
    }
}