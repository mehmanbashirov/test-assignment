package com.example.testassignment.controller;

import com.example.testassignment.model.dto.People;
import com.example.testassignment.model.dto.PeopleResponseDto;
import com.example.testassignment.service.PeopleService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("People controller unit tests")
@ExtendWith(SpringExtension.class)
@WebMvcTest(PeopleController.class)
class PeopleControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PeopleService peopleService;

    @Test
    void save() throws Exception {
        String NAME = "TEST";
        String URL = "http://test";

        List<People> peopleList = List.of(People.builder()
                .name(NAME)
                .url(URL)
                .build());

        var peopleResponseDto = PeopleResponseDto.builder()
                .count(1L)
                .peopleList(peopleList)
                .build();

        when(peopleService.getPeopleList(any())).thenReturn(peopleResponseDto);

        mvc.perform(
                get("/people")
                        .param("pageNo", "1")
                        .param("pageSize", "10")
                        .param("sortBy", "name")
                        .param("direction", "asc")
                        .param("name", "")
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.count").value(equalTo(1)));
    }

}