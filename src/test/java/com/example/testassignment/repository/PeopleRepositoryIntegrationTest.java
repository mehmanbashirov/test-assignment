package com.example.testassignment.repository;

import com.example.testassignment.model.entity.PeopleEntity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@ExtendWith(SpringExtension.class)
@DataJpaTest
@DisplayName("PeopleRepository integration tests")
class PeopleRepositoryIntegrationTest {


    @Autowired
    private PeopleRepository peopleRepository;

    @Test
    @DisplayName("Given PageRequest when findBy then should return list of PeopleEntity")
    public void getPeopleList() {
        Pageable pageable = PageRequest.of(0, 10);

        List<PeopleEntity> actual = peopleRepository.findBy(pageable);

        assertThat(actual.size()).isEqualTo(10);
    }

    @Test
    @DisplayName("Given PageRequest and Name when findByName then should return list of PeopleEntity")
    public void getPeopleListByName() {
        Pageable pageable = PageRequest.of(0, 10);

        List<PeopleEntity> actual = peopleRepository.findByNameContaining("Zia Simpson", pageable);

        assertThat(actual.size()).isEqualTo(2);
    }


}