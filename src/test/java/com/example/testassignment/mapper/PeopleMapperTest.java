package com.example.testassignment.mapper;

import com.example.testassignment.model.dto.People;
import com.example.testassignment.model.entity.PeopleEntity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNull;

@DisplayName("People mapper tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {PeopleMapperImpl.class})
class PeopleMapperTest {


    @Autowired
    private PeopleMapper peopleMapper;

    @Test
    @DisplayName("Given null as an argument when toPeopleDtoList then should return null")
    void toPeopleDtoListNullArgumentCase() {
        //given

        //when
        List<People> actual = peopleMapper.toPeopleDtoList(null);

        //then
        assertNull(actual);
    }

    @Test
    @DisplayName("Given List of PeopleEntity as an  argument when toPeopleDtoList then should return List of PeopleDto")
    void toPeopleDtoList() {
        //given
        String name = "TEST";
        String url = "http://test";

        var peopleEntity = PeopleEntity.builder()
                .id(1L)
                .name(name)
                .url(url)
                .build();

        List<People> expected = List.of(People.builder()
                .name(name)
                .url(url)
                .build());

        //when
        List<People> actual = peopleMapper.toPeopleDtoList(List.of(peopleEntity));

        //then
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    @DisplayName("")
    void toPeopleDtoNullCase() {
        //given

        //when
        People actual = peopleMapper.toPeopleDto(null);

        //then
        assertNull(actual);
    }


    @Test
    @DisplayName("Given PeopleEntity as an argument when toPeopleDto then should return PeopleDto")
    void toPeopleDto() {
        //given
        String name = "TEST";
        String url = "http://test";

        var peopleEntity = PeopleEntity.builder()
                .id(1L)
                .name(name)
                .url(url)
                .build();

        People expected = People.builder()
                .name(name)
                .url(url)
                .build();

        //when
        People actual = peopleMapper.toPeopleDto(peopleEntity);

        //then
        assertThat(actual).isEqualTo(expected);
    }
}