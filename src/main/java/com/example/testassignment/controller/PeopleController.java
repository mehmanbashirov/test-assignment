package com.example.testassignment.controller;

import com.example.testassignment.model.dto.PeopleResponseDto;
import com.example.testassignment.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/people")
@RequiredArgsConstructor
@Validated
public class PeopleController {

    private final PeopleService peopleService;

    @CrossOrigin
    @GetMapping
    public PeopleResponseDto findByName(@RequestParam(defaultValue = "0") Integer pageNo,
                                        @RequestParam(defaultValue = "10") Integer pageSize,
                                        @RequestParam(defaultValue = "id") String sortBy,
                                        @RequestParam(defaultValue = "asc") String direction,
                                        @RequestParam String name) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(Sort.Direction.fromString(direction), sortBy));

        if (ObjectUtils.isEmpty(name)) {
            return peopleService.getPeopleList(pageable);
        } else {
            return peopleService.findByName(name, pageable);
        }
    }

}
