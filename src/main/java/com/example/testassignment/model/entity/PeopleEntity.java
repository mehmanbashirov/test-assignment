package com.example.testassignment.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import static com.example.testassignment.model.entity.PeopleEntity.TABLE_NAME;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = TABLE_NAME)
public class PeopleEntity {

    public static final String TABLE_NAME = "PEOPLE";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_generator")
    @SequenceGenerator(name = "people_seq", sequenceName = "people_seq", allocationSize = 1)
    private Long id;

    private String name;

    private String url;

}
