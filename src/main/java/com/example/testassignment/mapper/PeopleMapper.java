package com.example.testassignment.mapper;

import com.example.testassignment.model.dto.People;
import com.example.testassignment.model.entity.PeopleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public abstract class PeopleMapper {

    public abstract List<People> toPeopleDtoList(List<PeopleEntity> peopleEntityList);

    public abstract People toPeopleDto(PeopleEntity entity);

}
