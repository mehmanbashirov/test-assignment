package com.example.testassignment.service;

import com.example.testassignment.mapper.PeopleMapper;
import com.example.testassignment.model.dto.People;
import com.example.testassignment.model.dto.PeopleResponseDto;
import com.example.testassignment.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PeopleService {

    private final PeopleRepository peopleRepository;
    private final PeopleMapper peopleMapper;


    public PeopleResponseDto getPeopleList(Pageable pageable) {
        Long peopleCount = peopleRepository.count();
        List<People> peopleList = peopleMapper.toPeopleDtoList(peopleRepository.findBy(pageable));

        return PeopleResponseDto.builder()
                .count(peopleCount)
                .peopleList(peopleList)
                .build();
    }

    public PeopleResponseDto findByName(String name, Pageable pageable) {
        Long peopleCount = peopleRepository.countByName(name);
        List<People> peopleList = peopleMapper.toPeopleDtoList(peopleRepository.findByNameContaining(name, pageable));

        return PeopleResponseDto.builder()
                .count(peopleCount)
                .peopleList(peopleList)
                .build();
    }

}
