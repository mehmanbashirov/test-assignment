package com.example.testassignment.repository;

import com.example.testassignment.model.entity.PeopleEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PeopleRepository extends JpaRepository<PeopleEntity, Long> {

    List<PeopleEntity> findByNameContaining(String name, Pageable pageable);

    List<PeopleEntity> findBy(Pageable pageable);

    Long countByName(String name);

}
