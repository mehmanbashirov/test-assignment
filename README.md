# Test Assignment Application

-  By executing the below command we will get postgres database with the name of schema "test_assignment". <br/>
 `docker-compose -f infra/dev/test-assignment-compose.yaml up -d`

-  Don not need to insert dictionary data bcz it will be loaded automatically when we start application(Bcz I have used liquibase).

-  To open application you should use the below url.
`http://localhost:8082/`

-  I am not good at frontend, I have just focused on backend side, that is why frontend side is not good enugh.



PS: If Docker have not been installed in your computer you can add any postgress datasource config(username, password, database) in application.yaml file then tables will be created, data will be loaded automatically.
